*** Settings ***

Library     SeleniumLibrary
Library     String

*** Variables ***
${Browser}      chrome
${HomePage}     automationpractice.com/index.php
${Scheme}   http
${automationURL}    ${Scheme}://${HomePage}

*** Keywords ***
Open HomePage
    Open Browser    ${automationURL}    ${Browser}

*** Test Cases ***
G001 Hacer Click En Contenedores
    Open HomePage
    Set Global Variable    @{nombrecontenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a        //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${nombredecontenedor}   IN      @{nombrecontenedores}
    \   Click Element      ${nombredecontenedor}
    \   Wait Until Element Is Visible      xpath=//*[@id="bigpic"]
    \   Click Element      xpath=//*[@id="header_logo"]/a/img
    Close Browser

G001 Nuevo Caso De Prueba
    Open HomePage